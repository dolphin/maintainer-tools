DRM Maintainer Tools
====================

This documentation covers the tools and workflows for maintaining and
contributing to the Linux kernel DRM subsystem's drm-misc, drm-intel
and drm-xe repositories. The intended audience is primarily the
maintainers and committers of said repositories, but the workflow
documentation may be useful for anyone interested in the kernel
graphics subsystem development.

The drm-misc, drm-intel and drm-xe repos are maintained using the same
tools and very similar workflows. All feed to the same testing and
integration tree, the drm-tip. The documentation here is mostly
shared, highlighting the differences in workflows where applicable.

Please see :ref:`contributing` as well as the `project home page`_ for more
information on how to collaborate on the documentation and tools.

.. _project home page: https://gitlab.freedesktop.org/drm/maintainer-tools/

Contents:

.. toctree::
   :maxdepth: 2

   getting-started
   repositories
   drm-tip
   drm
   drm-misc
   drm-intel
   drm-xe
   committer-guidelines
   maintainer-guidelines
   commit-access
   dim
   qf
   code-of-conduct
   CONTRIBUTING
   MAINTAINERS
   COPYING

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
