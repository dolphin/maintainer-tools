.. _contributing:

========================
Contributing and Contact
========================

Please use `merge requests`_ for contributions and file `bug reports and feature
requests`_ at the `project home page`_.

Please use the `dim-tools@lists.freedesktop.org`_ mailing list for general
questions and discussion about the tooling and documentation.

Please make sure your patches pass the build and self tests by running::

  $ make check

Merge your changes once you have an ack from :ref:`maintainers`.

.. _merge requests: https://gitlab.freedesktop.org/drm/maintainer-tools/-/merge_requests

.. _bug reports and feature requests: https://gitlab.freedesktop.org/drm/maintainer-tools/issues

.. _project home page: https://gitlab.freedesktop.org/drm/maintainer-tools/

.. _dim-tools@lists.freedesktop.org: https://lists.freedesktop.org/mailman/listinfo/dim-tools
